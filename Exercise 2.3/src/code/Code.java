package code;

import static com.jogamp.opengl.GL.GL_COLOR_BUFFER_BIT;
import static com.jogamp.opengl.GL2ES3.GL_QUADS;

import javax.swing.JFrame;

import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.Animator;

public class Code extends JFrame implements GLEventListener
{	private GLCanvas myCanvas;
	private int renderingProgram;
	private int vao[] = new int[1];

	private float rotation = 0.0f;
	private float scale = 1.0f;
	private float rotationInc = 0.01f;
	private float scaleInc = 0.02f;
	
	public Code()
	{	setTitle("Assignment 2- Part 3");
		setSize(400, 400);
		myCanvas = new GLCanvas();
		myCanvas.addGLEventListener(this);
		this.add(myCanvas);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Animator animtr = new Animator(myCanvas);
		animtr.start();
	}

	public void display(GLAutoDrawable drawable)
	{	GL4 gl = (GL4) GLContext.getCurrentGL();
		gl.glUseProgram(renderingProgram);
		
		
		rotation += rotationInc;
		
		if (scale >= 1.0f || scale <= 0.05f)
		{
			scaleInc *= -1;
		}
		
		scale += scaleInc;
		
		
		
		int squareRotation = gl.glGetUniformLocation(renderingProgram, "rotation");
		int squareScale = gl.glGetUniformLocation(renderingProgram, "scale");
	
		gl.glProgramUniform1f(renderingProgram, squareRotation, rotation);
		gl.glProgramUniform1f(renderingProgram, squareScale, scale);
		
		gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		gl.glClear(GL_COLOR_BUFFER_BIT);
		
		gl.glDrawArrays(GL_QUADS,0,4);
		
	}

	public void init(GLAutoDrawable drawable)
	{	GL4 gl = (GL4) GLContext.getCurrentGL();
		renderingProgram = Utils.createShaderProgram("src/code/vertShader.glsl", "src/code/fragShader.glsl");
		gl.glGenVertexArrays(vao.length, vao, 0);
		gl.glBindVertexArray(vao[0]);
	}

	public static void main(String[] args) { new Code(); }
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {}
	public void dispose(GLAutoDrawable drawable) {}
}